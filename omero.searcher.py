#!/usr/bin/python

# Author: Ivan E. Cao-Berg (icaoberg@scs.cmu.edu)
#
# Copyright (C) 2012 Murphy Lab
# Lane Center for Computational Biology
# School of Computer Science
# Carnegie Mellon University
#
# June 1, 2012 I. Cao-Berg  Updated error message when trying to import ricerca
# 			Checks for newer versions of the content database online
# June 1, 2012 Y. Yu Added default values to input parameters
# June 2, 2012 R.F. Murphy Add separate directories for protein and dna
#                          Support additional servers.  Use input value defaults.
# June 2, 2012 I. Cao-Berg Optimized execution of omero.searcher
# October 1, 2012 J. Bakal Converted input parameters to switches
# November 15, 2012 J.Bakal Added support for user-supplied content db and feature calculation code
# June 15, 2013  J. Bakal  Added support for multi-server searching and downsampling query image
# June 28, 2013  J. Bakal  Updated error messages, suppressed pyslic warnings
#                          Refactored/organized retrieveCDB function
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; either version 2 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
# For additional information visit http://murphylab.web.cmu.edu or
# send email to murphy@cmu.edu

version = '1.3'
print "OMERO.searcher Local Client version " + version

import os, sys, glob, urllib, warnings
from distutils.version import StrictVersion

FILESEP = '/'

try:
   import numpy
except:
   sys.exit("Unable to import numpy. Please install and try again.")

try:
   import scipy
except:
   sys.exit("Unable to import scipy. Please install and try again.")

#try:
#   import h5py
#except:
#   sys.exit("Unable to import h5py. Please install and try again.")

try:
   import getopt
except:
   sys.exit("Unable to import getopt. Please install and try again.")

try:
   from ricerca import content
except:
   sys.exit(
   '''Unable to import ricerca. 
   To download the git repository from github
   git clone https://icaoberg@github.com/icaoberg/OMERO.searcher.client.git
   or bitbucket
   git clone https://bitbucket.org/icaoberg/ricerca.git

   To manually download the latest version, visit
   https://murphylab.web.cmu.edu/software
   ''')


#pickle is a library for saving/loading binary data
try:
   import cPickle as pickle
except:
   import pickle

try:
   import mahotas as mh
   if not mh.mahotas_version.__version__=='0.6.6':
      sys.exit('Please install mahotas version 0.6.6 to make sure features calculated match those saved in the available content databases.')
except:
   sys.exit('Unable to import mahotas. Please install and try again.')


#pyslic is the python version of slic
try:
   import pyslic
except:
   sys.exit( "Unable to load pyslic. It should have been included in your distribution of OMERO.searcher Local Client." )


import warnings
warnings.filterwarnings('ignore',module='pyslic')


def getDatabases(allDBs):
   try:
      murphylab = 'http://murphylab.web.cmu.edu/software/searcher/Databases'
      url = urllib.urlretrieve( murphylab, 'databases' )
      file = open( 'databases', 'r' )
      databases = file.readlines()
      file.close()
      databases = [line.strip() for line in databases]
      if allDBs==None:
         print 'Remote databases available:'
         for line in databases:
            print '\t'+line.strip().replace('.pkl','')
   except:
      print "Unable to connect for databases saved at Murphy Lab website."

   localdbs=glob.glob('*.pkl')
   if len(localdbs)==0:
      print 'No local databases available.'
   else:
      if allDBs==None:
         print 'Local databases available:'
         for line in localdbs:
            print '\t'+line.strip().replace('.pkl','')
   print

   return databases,localdbs

def usage():
   print \
"""
Usage: python omero.searcher.py <options> proteindirectory [dnadirectory]
Options include:
    -s/--server <servername>         Specify server/s where images are located
    -f/--featureset <featuresetname> Specify featureset to be used
    -a/--alpha  <value>              Specify alpha value (as a float)
    -r/--resolution <value>          Specify image scale (as a float)
    -n/--numimages <value>           Specify maximum number of images to return
    -o/--output <path>               Specify filename for output file
    -d/--db                          Return list of available databases 
                                     (=server+featureset) and exit
    -t/--tolerance                   Specify downsample tolerance (as a float)
    -e/--everywhere                  Use all databases available
                                     "localDBs" in same directory 
                                     "globalDBs" from murphylab website
    -h/--help                        Print this help message and exit
"""
   return

def checkSearcherVersion():
   print 'Checking for new stable version... '
   try:
      murphylab = 'http://murphylab.web.cmu.edu/software/searcher/LocalClientVersion'
      url = urllib.urlretrieve( murphylab, 'version' )

      file = open( 'version', 'r' )
      latestVersion = file.readline()
      latestVersion = latestVersion[0:len(latestVersion)-1]
      file.close()

      if StrictVersion(latestVersion) > version:
         print 'A newer version ('+latestVersion+') is available.'
      else:
         print "Version is up to date."    
   except:
      print "Unable to check for new stable version at "+murphylab+"."



def retrieveCDB(server,fset,allDBs):

   def checkRemoteVersion(server,pklbase):
        try:
            server_url = 'http://'+server+'/searcher/'
            version_file = server.split('/')[0]+'_version'
            url = urllib.urlretrieve(server_url + 'Databases',version_file)
            file2 = open(version_file,'r')
            latestVersions2 = file2.readlines()
            file2.close()

            if "DOCTYPE HTML PUBLIC" in latestVersions2[0]:
                print "No list of available content databases found at \n"+server_url
                return -1

            dbversion_dict=dict((line.split('-v')[0], int(line.split('-v')[1].replace('.pkl','').strip())) for line in latestVersions2)
            if dbversion_dict.has_key(pklbase):
                return dbversion_dict[pklbase]
            else:
                print "This combination of server and slf - "+pklbase+" - is not found at \n"+server_url
                return -1
        except:
            print "Unable to download list of available content databases from \n"+server_url
            return -1


   def retrieveRemoteVersion(version,server,pklfilename):
       server_url = 'http://'+server+'/searcher/'
       print "Downloading version ("+str(version)+") of the content database from"
       print server_url

       try:
           urldb = urllib.urlretrieve(server_url+pklfilename,pklfilename)
           checkfile = open(pklfilename,'rb').read()
           if "<html>" in checkfile:
               os.remove(pklfilename)
               return False
           else:
               return True
       except:
           return False

   def getLatestLocalVersion(server):
      pklfiles = glob.glob( pklbase + "-v*" + ".pkl")
      if len(pklfiles)>0:
         pklfilename = pklfiles[len(pklfiles)-1]
         print "Using latest local copy: "+pklfilename
         return pklfilename
      else:
         return ''

   print 'Loading content database for featureset '+str(fset)+', server '+str(server)

   pklbase = server + "-" + fset
   serverVersion = -1
   murphyVersion = -1

   #check remote versions
   if not allDBs=='localDBs':
      serverVersion = checkRemoteVersion(server,pklbase)

      murphylab = 'murphylab.web.cmu.edu/software/'
      murphyVersion = checkRemoteVersion(murphylab,pklbase)

   if allDBs=='localDBs' or (serverVersion == -1 and murphyVersion == -1):
      pklfilename=getLatestLocalVersion(server)
   else:
      #choose version: if serverVersion and murphyVersion are the same, download from Murphy Lab
      if serverVersion > 0 and serverVersion > murphyVersion:
          useVersion = serverVersion
          useServer = server
      elif murphyVersion > 0:
          useVersion = murphyVersion
          useServer = murphylab

      pklfilename = pklbase + "-v" + str(useVersion) + ".pkl"

      if os.path.isfile(pklfilename):
          print 'Using local copy of content database, which is same as latest version, also available on '+useServer.split('/')[0]
          loadDatabase=True
      else:
          #download version from chosen server           
         loadDatabase=retrieveRemoteVersion(useVersion,useServer,pklfilename)

      if not loadDatabase:
         print "No local copy of version "+str(useVersion)+" of content database found."
         print "and unable to download from "+useServer.split('/')[0]
         pklfilename=getLatestLocalVersion(server)
         if pklfilename=='':
            sys.exit("No local copy of any version of content database found for "+server+" and "+fset+".")

   pklfile = open(pklfilename,'rb')
   database = pickle.load(pklfile)
   pklfile.close()

   return database


def combineMasterDBKeys(masterdb):
   masterKeys=masterdb.keys()
   masterKeys.remove('info')
   masterKeys.sort()
   deletedKeys=[]
   for key in masterKeys:
      if key not in deletedKeys:
         newKeys=[closekey for closekey in masterKeys if closekey!=key and closekey<key*1.1 and closekey>key*.9]
         for newKey in newKeys:
            masterdb[key]+=masterdb[newKey]
            del(masterdb[newKey])
            deletedKeys.append(newKey)
   return masterdb


def getImageRefs(args,num_channels_required):
   use_reference_channel = False

   if len(args)<>num_channels_required or len(args)==0:
      print str(num_channels_required)+' channels are required for feature calculation. '+str(len(args))+' channels provided.'
      sys.exit()
   elif len(args)==2:
      proteindirectory = args[0]
      dnadirectory = args[1]
      use_reference_channel = True
   elif len(args)==1:
      proteindirectory = args[0]

   if not os.path.exists(os.path.dirname(proteindirectory)):
      sys.exit("Primary directory cannot be empty")

   if num_channels_required==2 and not os.path.exists(os.path.dirname(dnadirectory)):
      sys.exit("Feature code requires reference image. DNA directory cannot be empty")

   print "Checking protein and DNA image lists"
   if os.path.isdir(proteindirectory):
      proteindirectory+='/*'
   proteinChannelFiles = glob.glob( proteindirectory )
   if not proteinChannelFiles:
      sys.exit("No protein channel images were found in " + proteindirectory )
   else:
      proteinChannelFiles.sort()

   imageRefs={}
   if use_reference_channel:
      if os.path.isdir(dnadirectory):
         dnadirectory+='/*'
      nuclearChannelFiles = glob.glob( dnadirectory )
      if not nuclearChannelFiles:
         sys.exit("No DNA channel images were found in " + dnadirectory )
      else:
         nuclearChannelFiles.sort()

      if len( proteinChannelFiles ) != len( nuclearChannelFiles ):
         sys.exit("Mismatch between number of nuclear and protein images")
   else:
      nuclearChannelFiles = ['']*len(proteinChannelFiles)

   for ind, val in enumerate(proteinChannelFiles):
      imageRefs[val]=[(scale,nuclearChannelFiles[ind]),1]

   print str(len(imageRefs)) + " images found"

   return imageRefs


def processIDs(cdb_row):
   ID=cdb_row[3]+'###'+cdb_row[4]+'###'+cdb_row[5]
   return ID


def processTestSet(contentDB,image_refs_dict,dscale):
   #DEFINE FEATURE FUNCTION FROM DATABASE                                                                              
   func_name = 'calc_'+contentDB['info']['slf_name']
   new_code_text = contentDB['info']['slf_code']
   exec(new_code_text)
   func=locals()[func_name]


   #CALCULATING FEATURES ON IMAGES PROVIDED                                                                            
   print "Calculating features for query set from local images"
   print 'Using supplied feature calculation function: '+func_name
   localfiles = []

   image_keys = image_refs_dict.keys()
   image_keys.sort()

   for protfilename in image_keys:
      dnafilename = image_refs_dict[protfilename][1]
      scale = image_refs_dict[protfilename][0]
      if dnafilename=='':
         features = func(protfilename, scale)
      else:
         features = func(protfilename, dnafilename, scale)

      localfiles.append( [protfilename, 1, list(features)] )

   return localfiles


def processTestSetwithDS(contentDB,imageRefs,dscale):
   try:
         import mahotas as mh
         if not mh.mahotas_version.__version__=='0.6.6':
            sys.exit('Please install mahotas version 0.6.6 to make sure features calculated match those saved in the available content databases.')
   except:
         sys.exit('Unable to import mahotas. Please install and try again.')

   class FeatureError(Exception):
      def __init__(self,value):
         self.value = value
      def __str__(self):
         return repr(self.value)

   def downsampleImage(baseImg,dsRate):
      from scipy import ndimage
      import numpy
      C=numpy.ones((dsRate,dsRate))
      img=ndimage.convolve(baseImg,C)
      img=img[::dsRate,::dsRate]
      return img

   #DEFINE FEATURE FUNCTION FROM DATABASE
   func_name = 'calc_'+contentDB['info']['slf_name']
   new_code_text = contentDB['info']['slf_code']
   exec new_code_text in locals()
   funcDS=locals()[func_name]

   #CALCULATING FEATURES ON IMAGES PROVIDED
   print "Calculating features for query set from local images"

   localfiles = []

   imageKeys = imageRefs.keys()
   imageKeys.sort()

   if contentDB['info'].has_key('downsample_tolerance'):
      dsTolerance=contentDB['info']['downsample_tolerance']
   else:
      dsTolerance=.10

   for protfilename in imageKeys:
      dnafilename = imageRefs[protfilename][1]
      scale = imageRefs[protfilename][0]
      protein_mh=mh.imread(protfilename)
      dsRate=float(dscale/scale)
      if abs(dsRate-1)<dsTolerance:
        if dnafilename=='':
           features = funcDS(protein_mh, dscale)
        else:
           dna_mh=mh.imread(dnafilename)
           features = funcDS(protein_mh, dna_mh, dscale)
      else:
         protein_mh=downsampleImage(protein_mh,dsRate)
         if dnafilename=='':
            features = funcDS(protein_mh,dscale)
         else:
            dna_mh=mh.imread(dnafilename)
            dna_mh=downsampleImage(dna_mh,dsRate)
            features = funcDS(protein_mh, dna_mh, dscale)
   
   if 'features' in locals():
      localfiles.append( [protfilename, 1, list(features)] )
      print 'Using supplied feature calculation function: '+func_name+' at scale: '+str(dscale)
      print 'there are '+str(len(localfiles[0][2]))+' features calculated'
      return localfiles
   else:
      raise FeatureError(protfilename)


maxNumberOfImgs = 100
servers = []
fset = ''
alpha = -5
output = ''
proteindirectory = None
dnadirectory = None
userDefined = False
allDBs = None
tolerance = 0

try:
    opts, args = getopt.getopt(sys.argv[1:], "dhs:e:f:a:r:n:o:t:", ["db","help","servers","everywhere=","featureset=","alpha=","resolution=","numimages=","output=","tolerance="])
except getopt.GetoptError, err:
    print str(err)
    usage()
    sys.exit(2)

if len(opts)==0 and len(args)==0:
   usage()
   sys.exit(2)

for o,a in opts:
   if o in ('-h','--help'):
      usage()
      sys.exit()
   elif o in ('-d','--db'):
      getDatabases(None)
      sys.exit()
   elif o in ('-s','--servers'):
      servers.append(a)
      print servers
   elif o in ('-f','--featureset'):
      fset=a
   elif o in ('-a','--alpha'):
      alpha=float(a)
   elif o in ('-r','--resolution'):
      scale=float(a)
   elif o in ('-n','--numimages'):
      maxNumberOfImgs=long(a)
   elif o in ('-o','--output'):
      output=a
   elif o in ('-e','--everywhere'):
      allDBs=a
   elif o in ('-t','--tolerance'):
      tolerance=float(a)
   else:
      assert False, 'unhandled option'


if allDBs == None:
   if servers==[]:
      print 'You must specify at least one server for the database or choose to use all local or global databases.'
      getDatabases(None)
      sys.exit()
else:
   if len(servers)>0:
      print 'You must either specify which database(s) you want to use or choose to use all local or global databases, but not both.'
      getDatabases(None)
      sys.exit()

if fset=='':
   print 'You must specify a featureset to use; featuresets are the value following the server name in the following database list, for example "slf34"'
   getDatabases(None)
   sys.exit()

if tolerance > .5:
   print 'Downsample tolerance must be less than .5'
   sys.exit()

if not output:
   if len(servers)>=1:
      strsep = '_'
      output = strsep.join(servers) + '-' + fset + '-results.html'
   if allDBs is not None:
      output = allDBs + '-' + fset + '-results.html'
      
if scale<=0:
   sys.exit("Image scale must be greater than 0")

checkSearcherVersion()

if allDBs in ['globalDBs','localDBs']:
   [globalDBs,localDBs]=getDatabases(allDBs)
   servers=[x.strip().split('-')[0] for x in locals()[allDBs] if fset in x]
   if servers==[]:
      print "There are no "+allDBs.replace('DBs','')+" content databases available for featureset "+fset+"."
      getDatabases(None)
      sys.exit()
elif allDBs is not None:
      sys.exit("Valid options for -e/everywhere are \"localDBs\" for content databases saved in the same directory as omero.searcher.py or \"globalDBs\" for content databases saved on the Murphy Lab website.")


#RETRIEVING CONTENT DATABASE(S)
print "Loading content database(s) (this may take a while)..."

masterDB={}
minTolerance=999
cdbNames=[]

for server in servers:
   database=retrieveCDB(server,fset,allDBs)
   if database['info'].has_key('downsample_tolerance'):
      minTolerance=min(minTolerance,database['info']['downsample_tolerance'])
   cdbNames.append(database['info']['name'])
   for key, val in database.items():
      if key != 'info' and masterDB.has_key(key):
         masterDB[key]+=val
      else:
         masterDB[key]=val

masterDB=combineMasterDBKeys(masterDB)
if tolerance > 0:
   masterDB['info']['downsample_tolerance']=tolerance
elif minTolerance < 1:
   masterDB['info']['downsample_tolerance']=minTolerance
else:
   masterDB['info']['downsample_tolerance']=.10


#CHECKING IMAGE(S) PROVIDED
try:
   num_channels_required = masterDB['info']['num_channels_required']
except:
   sys.exit('Database needs to be in dictionary format with number of required channels included.')

imageRefs=getImageRefs(args,num_channels_required)

"""
for key,val in masterDB.items():
   print key, len(val)

for key,val in masterDB['info'].items():
   print key
   print val
   print

print imageRefs
sys.exit()
"""

#RANKING 
results = content.rankingWrapperWithDownsample(masterDB, imageRefs, processIDs, processTestSetwithDS)
resultKeys=results.keys()
resultKeys.sort()

for key,val in results.items():
   if len(val)==0:
      sys.exit("Unable to calculate features on image/s selected. Please try again with new images.")


#CREATING HTML FILE
print "Making HTML results file"

htmlfile = open( output, "w" )
print >> htmlfile, "<html>"
print >> htmlfile, "<header>"
print >> htmlfile, "<title>OMERO.searcher results</title>"
print >> htmlfile, "</header>"
print >> htmlfile, "<body>"
print >> htmlfile, "<h1>OMERO.searcher results</h1>"
print >> htmlfile, "<table align=\'left\' border=0>"
print >> htmlfile, "<tr><td width=25%><b>Query Image(s)</b></td>"
for protimg,imginfo in imageRefs.items():
   print >> htmlfile, "<td>"+str(protimg)+"<br>"+imginfo[0][1]+"<p><img src=\'"+str(os.path.abspath(protimg))+"\' width=\'25%'/></td>"
print >> htmlfile, "</tr>" 
print >> htmlfile, "<tr><td><b>Content Database</b></td><td>" + str(cdbNames)+"</td></tr>" 
print >> htmlfile, "<tr><td><b>Original Scale</b></td><td>" + str(scale) + "</td></tr>" 
print >> htmlfile, "<tr><td><b>Comparison Scale/s</b></td><td>" + str(resultKeys) + "</td></tr>" 
print >> htmlfile, "<tr><td><b>Feature Set</b></td><td>" + str(fset) + "</td></tr>" 
print >> htmlfile, "<tr><td><b>Number to Retrieve</b></td><td>"+str(maxNumberOfImgs)+"</td></tr>"
print >> htmlfile, "</table>"
print >> htmlfile, "<br>"

print >> htmlfile, "<table align=\'left\' border=0 width=100%>"
print >> htmlfile, "<tr><td><br></td><tr>"
print >> htmlfile, "<tr><td width=25%><b>Results</b></td><td></td></tr>"
print >> htmlfile, "</table>"
print >> htmlfile, "<br>"
print >> htmlfile, "<p>"
if 'omepslid2.compbio.cs.cmu.edu' in servers: 
   print >> htmlfile, " (if images are not displayed, click on an info link to log into the OMERO database - the username is demo_nm and the password nature@u0816 - use the back button on your browser to return to this page and then hit refresh)"
print >> htmlfile, "<p>"
print >> htmlfile, "<table align=\'left\' border=\'1\' width=100%>"
print >> htmlfile, "<tr align=\'center\'>"
for scale in resultKeys:
   print >> htmlfile, "<th colspan=2>"+str(scale)+"</th>"
print >> htmlfile, "</tr>"
print >> htmlfile, "<tr align=\'center\'>"
for scale in resultKeys:
   print >> htmlfile, "<th>Thumbnail</th>"
   print >> htmlfile, "<th>Image Data</th>"
print >> htmlfile, "</tr>"

for index in range(0,maxNumberOfImgs):
   print >> htmlfile, "<tr>"
   for scale in resultKeys:
      info_url=results[scale][index].split('###')[0]
      thumb_url=results[scale][index].split('###')[1]
      thumb_src=results[scale][index].split('###')[2]

      print >> htmlfile, "<td align=\'center\'><a href=\'"+thumb_url+"\'><img src=\'"+thumb_src+"\' width='25%'></td>" 
      print >> htmlfile, "<td align=\'center\'><a href=\'" +info_url + "\'>Info</a></td>"     

   print >> htmlfile, "</tr>"

print >> htmlfile, "</table>"
print >> htmlfile, "</body>"
print >> htmlfile, "</html>"
htmlfile.close()
